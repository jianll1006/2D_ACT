﻿namespace TwlPhy
{
    using System;
    /// <summary>
    /// 2D矩形
    /// 俯视图观察场景
    /// 坐标x,y表示 大小length width
    /// 3D长方体
    /// 坐标x,y,z表示 大小length width height 
    /// </summary>
    public class Box2D
    {
        public override string ToString()
        {
            return string.Format("centerX: {0}  centerX: {1}  length: {2}  width: {3}", centerX, centerX, length, width);
        }
        //2d x,y 坐标 
        public float centerX
        {
            protected set;
            get;
        }
        public float centerY
        {
            protected set;
            get;
        }

        public float length
        {
            protected set;
            get;
        }
        public float width
        {
            protected set;
            get;
        }


        public Box2D(float x, float y, float l, float w)
        {
            centerX = x;
            centerY = y;
            length = l;
            width = w;
            refresh();
        }

        //BOX的锚点在中心
        protected virtual void refresh()
        {
            left = centerX - length / 2;
            right = centerX + length / 2;
            top = centerY + width / 2;
            bottom = centerY - width / 2;
        }

        protected float left;
        public float Left
        {
            get
            {
                return left;
            }
        }

        protected float right;
        public float Right
        {
            get
            {
                return right;
            }
        }

        protected float top;
        public float Top
        {
            get
            {
                return top;
            }
        }

        protected float bottom;
        public float Bottom
        {
            get
            {
                return bottom;
            }
        }

    }

}