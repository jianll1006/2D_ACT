﻿namespace TwlPhy
{
    using System;

    public class DymicBox2D : Box2D
    {
        public DymicBox2D(float x, float y, float l, float w) : base(x, y, l, w)
        {

        }

        //重写 锚点在下方
        //protected override void refresh()
        //{
        //    left = centerX - length / 2;
        //    right = centerX + length / 2;
        //    top = centerY + width;
        //    bottom = centerY;
        //}

        public void UpdateCenter(float x, float y)
        {
            centerX = x;
            centerY = y;
            refresh();
        }

        public void OnlyUpdateCenter(float x, float y)
        {
            centerX = x;
            centerY = y;
        }

        public void AddCenter(float x, float y)
        {
            centerX += x;
            centerY += y;
            refresh();
        }

        public void UpdateSize(float l, float w)
        {
            length = l;
            width = w;
            refresh();
        }

    }
}
