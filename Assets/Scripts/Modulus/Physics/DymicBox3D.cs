﻿namespace TwlPhy
{
    using System;

    public class DymicBox3D : DymicBox2D
    {
        public float centerZ
        {
            protected set;
            get;
        }
        public float height
        {
            protected set;
            get;
        }

        public DymicBox3D(float x, float y, float l, float w) : base(x, y, l, w)
        {

        }

        public DymicBox3D(float x, float y, float z, float l, float w, float h) : base(x, y, l, w)
        {
            centerZ = z;
            height = h;

            refreshPosZ();
        }

        //用其他的碰撞盒复制+偏移 x,y坐标偏移z高度偏移
        // this(copy.centerX + offset.x, copy.centerY + offset.y, copy.width, copy.height, copy.MaxH + offset.z)
        public DymicBox3D(DymicBox3D copy, Vector3 offset) : this(copy.centerX + offset.x, copy.centerY + offset.y, copy.centerZ + offset.z, copy.length, copy.width, copy.height)
        {

        }

        //BOX的锚点在中心
        public void updatePosHeight(float posZ)
        {
            centerZ = posZ;
            refreshPosZ();
        }

        private void refreshPosZ() {
            maxH = centerZ + height;
            minH = centerZ;
        }

        protected float minH;
        public float MinH
        {
            get
            {
                return minH;
            }
        }

        protected float maxH;
        public float MaxH
        {
            get
            {
                return maxH;
            }
        }

        public string formatMoveBox() {
            string str = "left: {0}  right: {1}  top: {2}  bottom: {3}";
            return string.Format(str,Left,Right,Top,Bottom);
        }

        public string formatHitBox()
        {
            string str = "left: {0}  right: {1}  maxH: {2}  minH: {3}";
            return string.Format(str, Left, Right, MaxH, MinH);
        }

        public string formatBox()
        {
            string str = "left: {0}  right: {1}  top: {2}  bottom: {3}  maxH: {4}  minH: {5}";
            return string.Format(str, Left, Right, Top, Bottom,MaxH, MinH);
        }
    }
}
