﻿using System;
using UnityEngine;
using Pool;

public class man : PoolObject
{
    public int age = 0;
    public string name = "man";
}

public class woman : PoolObject
{
    public int money = 1;
    public string son = "woman";
}

public class TestClass : MonoBehaviour
{
    public int num = 0;
    private bool isStart = false;
    private double time = 0;

    private void Start()
    {
        Application.targetFrameRate = 120;
    }


    private void Update()
    {
        Application.targetFrameRate = 120;
        if (Input.GetKeyDown(KeyCode.P))
        {
            isStart = true;
            num = 0;
            time = TimerUtils.getMillTimer();
        }
        if (isStart)
        {
            if (TimerUtils.getMillTimer() - time >= 1000)
            {
                Debug.LogError(num);
                isStart = false;
                return;
            }
            num++;

        }

    }


}

