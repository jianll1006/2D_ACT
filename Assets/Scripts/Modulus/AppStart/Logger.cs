﻿using System;
using System.Collections.Generic;

public class Logger
{
    public static void log(string str) {
        UnityEngine.Debug.Log(str);
    }

    public static void logError(string str)
    {
        UnityEngine.Debug.LogError(str);
    }

}

