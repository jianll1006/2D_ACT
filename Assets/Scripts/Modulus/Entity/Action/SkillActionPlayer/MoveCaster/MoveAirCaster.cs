﻿namespace TwlAction
{
    using System;
    using Config;

    public class MoveAirCaster : MoveBaseCaster
    {
        public override void castAction(ActionData actionData, MoveConfig moveConfig)
        {
            BaseEntity role = EntityMgr.getRole<BaseEntity>(actionData.ownerId);
            if (role != null) {
                FSMArgs args = new FSMArgs();
                args.cfgId = moveConfig.airForce;
                role.transFsm(FSM_Flag.AirHit, args, true);
            }            
        }


    }
}
