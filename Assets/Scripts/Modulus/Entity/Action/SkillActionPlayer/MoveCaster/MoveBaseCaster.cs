﻿namespace TwlAction
{
    using Config;
    using System;

    public abstract class MoveBaseCaster
    {
        public abstract void castAction(ActionData actionData, MoveConfig moveConfig);
    }
}
