﻿namespace TwlAction
{
    using System;
    using System.Collections.Generic;

    public abstract class BaseActionPlayer
    {
        public BaseActionPlayer() {
            initialize();
        }
        public abstract void initialize();

        public abstract void playAction(ActionData actionData,int cfgId);
    }
}
