﻿namespace TwlAction
{
    using System;
    using System.Collections.Generic;
    using Config;

    public abstract class DamageBaseCaster
    {
        public abstract void castAction(ActionData actionData, AttackConfig attackConfig);

    }
}
