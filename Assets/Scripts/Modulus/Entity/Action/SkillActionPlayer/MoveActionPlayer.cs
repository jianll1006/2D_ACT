﻿namespace TwlAction
{
    using System;
    using System.Collections.Generic;
    using Config;
    using TwlPhy;

    /// <summary>
    /// 位移播放器
    /// 位移类型
    /// 1.普攻受击 效果:玩家站立 击退 玩家浮空 浮空 玩家倒地 浮空 
    /// 2.浮空受击
    /// 3.曲线移动
    /// </summary>
    public class MoveActionPlayer : BaseActionPlayer
    {
        private Dictionary<int, MoveBaseCaster> movePool;
        public override void initialize()
        {
            movePool = new Dictionary<int, MoveBaseCaster>();

            movePool.Add(1, new MoveNormalCaster());
            movePool.Add(2, new MoveAirCaster());
            movePool.Add(3, new MoveCurveCaster());
        }

        public override void playAction(ActionData actionData, int cfgId)
        {
            MoveConfig cfg = MoveConfig.get(cfgId);
            if (cfg != null) {
                MoveBaseCaster caster;
                if (movePool.TryGetValue(cfg.moveType, out caster)) {
                    caster.castAction(actionData, cfg);
                }             
            }
        }



    }
}
