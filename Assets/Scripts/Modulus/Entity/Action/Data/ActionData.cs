﻿namespace TwlAction
{
    using System;
    using System.Collections.Generic;
    using TwlPhy;

    public class ActionData
    {
        public int actionId;//模版Id
        public int actionGuid;//唯一id
        public long ownerId;//归属者Id
        public Vector3 ownerPos;
        //public Vector3 size = new Vector3(1, 1, 1);
        //public string resName = @"AssetBundle\Prefabs\model\RoleBullet\bullet";
    }
}
