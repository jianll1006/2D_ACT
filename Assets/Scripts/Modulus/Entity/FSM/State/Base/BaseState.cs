﻿using System;
using System.Collections.Generic;

public class BaseState
{
    protected float minFlySpeed = 0.1f;//基础每帧高度
    protected float speedDamping = 0.005f;//浮空速度衰减
    protected float reboundHeight = 2f;//反弹临界值
    protected float reboundForce = 0.7f;//反弹力度

    public FSM_Flag Flag
    {
        get; set;
    }
    public BaseEntity Agent
    {
        get; set;
    }

    public virtual bool allow(FSM_Flag flag) {
        return true;
    }

    public virtual void onEnter(FSMArgs args = null)
    {

    }

    public virtual void onRefresh(FSMArgs args = null) { 

    }

    public virtual void onTick()
    {

    }

    public virtual void onRender()
    {

    }

    public virtual void onExit()
    {

    }

    internal protected void forceToIdle() {
        Agent.transFsm(FSM_Flag.Idle, null, true);
    }

}

