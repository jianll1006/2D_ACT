﻿using System;
using System.Collections.Generic;
using TwlPhy;

public class MainPlayerRunState : BaseState
{
    Vector2 dir = Vector2.zero;
    private int runFrame = 0;

    public override void onEnter(FSMArgs args = null)
    {
        this.Agent.playAnim("run", true);
        SyncMoveUtils.syncMove(this.Agent.UID);
    }

    public override void onTick()
    {

    }

    public override void onRender()
    {
        float dis = 0;
        float dst = Agent.speed * Physics2D.logicFrame;
        bool isKnock = Physics2D.moveCast(Agent.DyBox, Agent.moveDir, dst, ref dis); //Physics2D.isCollision(Agent.DyBox, Agent.moveDir, dst, ref dis);
        //Logger.log(string.Format("<color=red>dst {0} dis {1}</color>", dst, dis));
        Agent.doMove(Agent.moveDir * dis);
        //渲染
        Agent.updateRenderOrder();
        //同步
        runFrame++;
        if (runFrame >= Physics2D.syncIntervalFrame)
        {
            runFrame = 0;
            SyncMoveUtils.syncMove(this.Agent.UID);
        }
    }

    public override void onExit()
    {
        //syncMove(Vector2.zero, 3);
        //退出停止移动
        SyncMoveUtils.syncMove(Agent.UID, 2);
    }

    public override bool allow(FSM_Flag flag)
    {
        return flag != this.Flag;
    }
}

