﻿using System;
using System.Collections.Generic;
using TwlPhy;
using Config;

/// <summary>
/// 受击浮空状态
/// 
/// </summary>
public class AirHitState : BaseState
{
    protected int nowForce = 0;//当前浮空力度
    protected float weight = 1f;//角色体重(萝莉轻 大叔重)
    protected MoveConfig moveCfg;

    public override void onEnter(FSMArgs args = null)
    {
        executeArgs(args);
    }
    public override void onRefresh(FSMArgs args = null)
    {
        executeArgs(args);
    }

    protected void executeArgs(FSMArgs args)
    {
        int cfgForce = args.cfgId;
        if (nowForce <= cfgForce)
        {
            nowForce = cfgForce;
        }
    }

    protected virtual void flyMove()
    {
        float add = (minFlySpeed * (1 + nowForce * speedDamping)) * weight;
        Agent.updatePosHeight(Agent.DyBox.centerZ + add);
        //Agent.doMove(Vector2.up * add);
        nowForce -= 1;
    }

    //浮空应该是力度判断
    public override void onTick()
    {
        flyMove();
        if (nowForce <= 0)
        {
            //这里处理落下
            Agent.transFsm(FSM_Flag.AirFall, null, true);
        }
    }

}

