﻿using System;
using UnityEngine;

public class CameraUtils
{
    public static void setCameraObj(GameObject player)
    {
        if (player == null)
        {
            return;
        }
        GameObject parent = null;
        MainCameraWidget mw = null;
        if (GameObject.FindGameObjectWithTag("PlayerCamera") != null)
        {
            parent = GameObject.FindGameObjectWithTag("PlayerCamera");
        }
        else
        {
            Camera main = Camera.main;
            if (main == null)
            {
                main = new Camera();
                main.tag = "Main Camera";
            }

            if (main != null)
            {
                parent = new GameObject("PlayerCamera");
                parent.tag = "PlayerCamera";
                main.transform.SetParent(parent.transform);
                main.transform.localPosition = Vector3.zero;
                main.transform.localEulerAngles = Vector3.zero;
                main.transform.localScale = Vector3.one;
            }
        }

        if (parent != null)
        {
            mw = parent.gameObject.GetComponent<MainCameraWidget>();
            if (mw == null)
            {
                mw = parent.gameObject.AddComponent<MainCameraWidget>();
            }
        }
        if (mw != null)
        {
            mw.setFollow(player);
        }
    }
    public static void setCameraPlayer(int playerId)
    {
        BaseEntity role = EntityMgr.getRole<BaseEntity>(playerId);
        if (role == null)
        {
            return;
        }
        setCameraObj(role.ShowWidget.Obj);
    }
    public static void doShake(float time, float att, float hor, float ver)
    {
        GameObject camera = GameObject.FindGameObjectWithTag("PlayerCamera");
        if (camera != null)
        {
            MainCameraWidget mw = camera.gameObject.GetComponent<MainCameraWidget>();
            if (mw != null)
            {
                mw.doShake(time, att, hor, ver);
            }
        }
    }

}

