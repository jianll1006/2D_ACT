﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityPool;

/// <summary>
/// 基础显示组件 只加载模型
/// 1.实体显示
/// 2.技能显示
/// 3.buff显示
/// </summary>
public class BaseDisplayWidget : MonoBehaviour
{
    private string resName;
    //访问transform开销也是有点大 客户端直接访问 服务器没有这个
    private Transform trans;
    public Transform Trans
    {
        get
        {
            if (trans == null)
            {
                trans = this.transform;
            }
            return trans;
        }
    }
    private GameObject obj;
    public GameObject Obj
    {
        get
        {
            if (obj == null)
            {
                obj = this.gameObject;
            }
            return obj;
        }
    }
    //加载状态
    private E_EntityLoadState loadState = E_EntityLoadState.Waiting;
    public E_EntityLoadState LoadState
    {
        get
        {
            return this.loadState;
        }
    }
    //资源
    private GameObject prefab = null;
    public GameObject Prefab
    {
        get
        {
            return this.prefab;
        }
        set
        {
            this.prefab = value;
        }
    }

    //设置渲染层
    protected int maxOrder = 1000;
    protected int magnifyOrder = 100;//放大倍率
    public virtual void updateRenderOrder()
    {
        //if (this.Render != null)
        //{
        //    float y = agent.DyBox.centerY - this.agent.Height;
        //    int order = y > 0 ? maxOrder - Mathf.CeilToInt(y * magnifyOrder) : maxOrder + Mathf.CeilToInt(y * magnifyOrder) * -1;
        //    this.Render.sortingOrder = order;
        //}
    }

    /// <summary>
    /// 加载模型资源
    /// @"AssetBundle\Prefabs\model\role_superman\model\role_superman"
    /// </summary>
    protected void loadModel(string res)
    {
        //如果资源名不同 需要加载
        if (this.resName != res)
        {
            //如果在加载中 取消加载
            if (this.LoadState == E_EntityLoadState.Loading)
            {
                UnityPool.PoolMgr.Instance.unLoad(resName, loadFinishEnd);
            }
            //如果加载完成 回收模型
            else if (this.LoadState == E_EntityLoadState.Finish)
            {
                UnityPool.PoolMgr.Instance.recyleObj(this.Prefab);
                this.Prefab = null;
            }
            //进行加载
            this.loadState = E_EntityLoadState.Loading;
            resName = res;
            UnityPool.PoolMgr.Instance.getObj(resName, loadFinishEnd, E_PoolMode.Time, E_PoolType.Model);
        }
    }

    void loadFinishEnd(GameObject go)
    {
        this.prefab = go;
        this.loadState = E_EntityLoadState.Finish;
        go.transform.SetParent(this.Trans);
        go.transform.localPosition = Vector3.zero;
        go.transform.localScale = Vector3.one;
        updateRenderOrder();
        loadFinish();
    }

    void Start()
    {
        onStart();
    }

    protected virtual void onStart()
    {
   
    }

    protected virtual void loadFinish()
    {

    }

    public virtual void refresh()
    {

    }

    public void onDispose()
    {
        if (this.LoadState == E_EntityLoadState.Loading)
        {
            UnityPool.PoolMgr.Instance.unLoad(resName, loadFinishEnd);
        }
        //回收资源
        if (this.Prefab != null) {
            UnityPool.PoolMgr.Instance.recyleObj(this.Prefab);
            this.Prefab = null;
        } 
        GameObject.Destroy(this.Obj);
    }

}



