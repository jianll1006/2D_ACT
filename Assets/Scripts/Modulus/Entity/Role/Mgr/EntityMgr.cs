﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EntityMgr : Singleton<EntityMgr>
{
    private Dictionary<long, BaseEntity> rolePool = null;
    public long mainRoleId = 0;

    protected override void initialize()
    {
        rolePool = new Dictionary<long, BaseEntity>();
    }

    private T get<T>(long uid) where T : BaseEntity, new()
    {
        if (rolePool.ContainsKey(uid))
            return rolePool[uid] as T;
        return null;
    }

    private T create<T>(RoleData data) where T : BaseEntity, new()
    {
        if (data == null) return null;
        if (rolePool.ContainsKey(data.uid)) return null;
        T role = new T();
        role.RoleData = data;
        rolePool.Add(data.uid, role);   
        return role;
    }

    private void remove(long uid)
    {
        if (rolePool.ContainsKey(uid))
        {
            BaseEntity entity = rolePool[uid];
            rolePool.Remove(uid);
            entity.onDispose();
        }
    }

    public void tick()
    {
        var ier = rolePool.GetEnumerator();
        while (ier.MoveNext())
        {
            ier.Current.Value.Render();
            ier.Current.Value.Tick();
        }
        ier.Dispose();
    }

    /// <summary>
    /// 创建
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="data"></param>
    /// <returns></returns>
    public static T createRole<T>(RoleData data) where T : BaseEntity, new()
    {
        return Instance.create<T>(data);
    }

    /// <summary>
    /// 获取
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="uid"></param>
    /// <returns></returns>
    public static T getRole<T>(long uid) where T : BaseEntity, new()
    {
        return Instance.get<T>(uid);
    }

    /// <summary>
    /// 获取主角
    /// </summary>
    /// <returns></returns>
    public static MainPlayer getMainRole()
    {
        return Instance.get<MainPlayer>(Instance.mainRoleId);
    }

    /// <summary>
    /// 获取所有实体
    /// </summary>
    /// <returns></returns>
    public static Dictionary<long, BaseEntity> getPool()
    {
        return Instance.rolePool;
    }

    /// <summary>
    /// 移除实体
    /// </summary>
    /// <param name="uid"></param>
    public static void removeRole(long uid)
    {
        Instance.remove(uid);
    }

    /// <summary>
    /// 移除所有
    /// </summary>
    public static void removeAll()
    {
        for (int i = 0; i < Instance.rolePool.Count; i++)
        {
            removeRole(Instance.rolePool[i].UID);
        }
    }

}

