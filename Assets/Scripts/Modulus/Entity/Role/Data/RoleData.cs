﻿using System;
using Pool;
using TwlPhy;

[System.Serializable]
public class RoleData : PoolObject
{
    public long uid;
    public string nickName;
    public string resName;
    //实体位置
    public Vector3 pos = new Vector3(0, -3 , 0);
    //移动碰撞盒
    public Vector3 moveBox = new Vector3(0.3f, 0.1f, 0.1f);
    //实体速度
    public float speed = 3;
    public float jumpSpeed = 3;
    //缩放
    public Vector3 scale = new Vector3(1, 1, 1);
    public Role_Type roleType = Role_Type.None;
    //受击碰撞盒
    public Vector3 hitBox = new Vector3(0.1f, 0.2f, 6f);
    //面向
    public LookFlag lookFlag = LookFlag.Right;
}

