using System.Collections.Generic;
using ConfigMap;
using Config;
namespace Config
{
public class BulletConfig
{
   public int  id;
   public int  moveType;
   public int  liveFrame;
   public float  flySpeed;
   public float[]  size;
   public int  actionId;
   public string  resName;

   public BulletConfig (int id,int moveType,int liveFrame,float flySpeed,float[] size,int actionId,string resName){
     this.id = id;
     this.moveType = moveType;
     this.liveFrame = liveFrame;
     this.flySpeed = flySpeed;
     this.size = size;
     this.actionId = actionId;
     this.resName = resName;

   }
   public static BulletConfig get(int key)
   {
      return BulletConfigMap.Instance.get(key);
   }
   public static Dictionary<int, BulletConfig> getAll()
   {
      return BulletConfigMap.Instance.getAll();
   }
   }
}
namespace ConfigMap
{
public class BulletConfigMap: Singleton<BulletConfigMap>
{
   public Dictionary<int, BulletConfig> map = null; 
   protected override void initialize() 
   {
      map = new Dictionary<int, BulletConfig>(); 
      map.Add(1001,new BulletConfig(1001,6,30,0.6f,new float[] {0.5f,0.5f,0.5f},500001,"AssetBundle/Prefabs/model/RoleBullet/bullet"));
      map.Add(1002,new BulletConfig(1002,6,30,1f,new float[] {0.5f,0.5f,0.5f},500002,"AssetBundle/Prefabs/model/RoleBullet/bullet"));
}
   public BulletConfig get(int key)
   {
      BulletConfig cfg;
     if (map.TryGetValue(key, out cfg))
   {
        return cfg;
   }
   return null;
   }
   public  Dictionary<int, BulletConfig> getAll()
   {
      return map;
   }
}
}
