using System.Collections.Generic;
using ConfigMap;
using Config;
namespace Config
{
public class MoveConfig
{
   public int  id;
   public int  moveType;
   public int  liveFrame;
   public int  force;
   public float  forceSpeed;
   public int  airForce;

   public MoveConfig (int id,int moveType,int liveFrame,int force,float forceSpeed,int airForce){
     this.id = id;
     this.moveType = moveType;
     this.liveFrame = liveFrame;
     this.force = force;
     this.forceSpeed = forceSpeed;
     this.airForce = airForce;

   }
   public static MoveConfig get(int key)
   {
      return MoveConfigMap.Instance.get(key);
   }
   public static Dictionary<int, MoveConfig> getAll()
   {
      return MoveConfigMap.Instance.getAll();
   }
   }
}
namespace ConfigMap
{
public class MoveConfigMap: Singleton<MoveConfigMap>
{
   public Dictionary<int, MoveConfig> map = null; 
   protected override void initialize() 
   {
      map = new Dictionary<int, MoveConfig>(); 
      map.Add(1001,new MoveConfig(1001,1,20,1,0.6f,16));
      map.Add(1002,new MoveConfig(1002,2,0,1,0.2f,0));
      map.Add(1003,new MoveConfig(1003,3,30,0,0f,30));
      map.Add(1004,new MoveConfig(1004,2,0,0,0f,50));
}
   public MoveConfig get(int key)
   {
      MoveConfig cfg;
     if (map.TryGetValue(key, out cfg))
   {
        return cfg;
   }
   return null;
   }
   public  Dictionary<int, MoveConfig> getAll()
   {
      return map;
   }
}
}
