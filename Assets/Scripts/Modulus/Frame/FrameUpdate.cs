﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class FrameUpdate : MonoBehaviour
{
    private static FrameUpdate _instance;
    public static FrameUpdate Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("FrameUpdate");
                GameObject.DontDestroyOnLoad(go);
                _instance = go.AddComponent<FrameUpdate>();
            }
            return _instance;
        }
    }

    private float runTime = 0;
    private float nextFrameTime = 0;
    private float frameLen = 0.02f;

    public void init()
    {
        frameLen = TwlPhy.Physics2D.logicFrame;
    }

    void Start()
    {
        nextFrameTime += Time.deltaTime;
    }

    private void Update()
    {
        runTime += Time.deltaTime;
        while (runTime > nextFrameTime) {
            nextFrameTime += frameLen;
            FrameMgr.AddFrame();
            EntityMgr.Instance.tick();
            TwlAction.ActionMgr.Instance.tick();
        }
    }
}

