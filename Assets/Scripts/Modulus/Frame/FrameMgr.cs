﻿using System;
using UnityEngine;

/// <summary>
/// 逻辑帧 先做单机
/// 1000毫秒 / 30 = 33
/// Update每帧检查客户端当前时间 计算出当前走到了哪一帧
/// 
/// </summary>
public class FrameMgr : Singleton<FrameMgr>
{
    private int nowFrame = 0;

    public static void AddFrame()
    {
        Instance.nowFrame++;
    }

    public static int NowFrame
    {
        get
        {
            return Instance.nowFrame;
        }
    }

}
