﻿namespace TwlEvent
{
    using System;
    using System.Collections.Generic;

    public class Message
    {
        public int intParam1;
        public int intParam2;
        public long longParam1;
        public long longParam2;
        public string strParam1;
        public string strParam2;


        public void dispatch(string name) {
            EventMgr.Instance.dispatch(name, this);
        }

    }
}
