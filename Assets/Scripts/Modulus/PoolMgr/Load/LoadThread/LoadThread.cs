﻿using UnityEngine;

public class LoadThread : MonoBehaviour
{
    private static LoadThread _instance;
    public static LoadThread Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("LoadThread");
                GameObject.DontDestroyOnLoad(go);
                _instance = go.AddComponent<LoadThread>();
            }
            return _instance;
        }
    }

    public void init()
    {

    }

}

